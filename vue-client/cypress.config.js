const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: "hy11r1",
  component: {
    devServer: {
      framework: "vue-cli",
      bundler: "webpack",
    },
  },

  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
