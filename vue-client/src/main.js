import Vue from 'vue'
import vuetify from '@/plugins/vuetify' // path to vuetify export
import App from './App.vue';

new Vue({
  render: h => h(App),
  vuetify,
}).$mount('#app');

Vue.config.productionTip = false;
