const https = require('https');
const config = require('./config/config');
const DOMParser = require('xmldom').DOMParser;
// Replace AWS SDK v2 with AWS SDK v3 imports
const { S3Client, GetObjectCommand, PutObjectCommand } = require('@aws-sdk/client-s3');
const { CloudFrontClient, CreateInvalidationCommand } = require('@aws-sdk/client-cloudfront');
const util = require('./util');

// Create SDK clients with AWS SDK v3
// Use region from environment variables or fall back to us-west-2
const region = process.env.AWS_REGION || 'us-west-2';

// Create S3 client with the correct regional endpoint
const s3Client = new S3Client({ 
    region,
    // Remove forcePathStyle as it might be causing issues
    credentials: {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
    },
    // Using the endpoint from the error message
    endpoint: `https://s3.${region}.amazonaws.com`
});

const cloudfrontClient = new CloudFrontClient({ 
    region,
    credentials: {
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
    }
});

let yieldsUpdatedCallback;

exports.YieldSpread = [];
exports.onYieldsUpdated = function (callback) {
    yieldsUpdatedCallback = callback;
};


function parseYields(yieldXML) {
    const YIELD_ELEMENTS = [{duration: 1, tagName: "BC_1MONTH"}, {duration: 3, tagName: "BC_3MONTH"},
        {duration: 6, tagName: "BC_6MONTH"}, {duration: 12, tagName: "BC_1YEAR"},
        {duration: 24, tagName: "BC_2YEAR"}, {duration: 36, tagName: "BC_3YEAR"},
        {duration: 60, tagName: "BC_5YEAR"}, {duration: 84, tagName: "BC_7YEAR"},
        {duration: 120, tagName: "BC_10YEAR"}, {duration: 240, tagName: "BC_20YEAR"},
        {duration: 360, tagName: "BC_30YEAR"}];

    // Check if XML is empty or invalid
    if (!yieldXML || yieldXML.trim() === '') {
        console.log('Error: Empty or invalid XML received');
        return null;
    }
    
    try {
        const doc = new DOMParser().parseFromString(yieldXML, 'text/xml');
        if (!doc) {
            console.log('Error: Failed to parse XML document');
            return null;
        }
        
        const newDateArray = doc.getElementsByTagName("G_NEW_DATE");
        if (!newDateArray || newDateArray.length === 0) {
            console.log('Error: No G_NEW_DATE elements found in XML');
            return null;
        }
        
        const latestDateElement = newDateArray[newDateArray.length - 1];
        const dateElement = latestDateElement.getElementsByTagName("NEW_DATE")[0];
        if (!dateElement || !dateElement.childNodes || !dateElement.childNodes[0]) {
            console.log('Error: No NEW_DATE element found');
            return null;
        }
        
        const splitDate = dateElement.childNodes[0].data.split('-');
        const date = new Date(Date.UTC(parseInt(splitDate[2], 10), parseInt(splitDate[0], 10) - 1, parseInt(splitDate[1], 10)));
        const yields = [];

        yields.push(date.getTime());
        
        // Check each yield element
        let missingElements = false;
        YIELD_ELEMENTS.forEach(element => {
            const yieldElement = latestDateElement.getElementsByTagName(element.tagName)[0];
            if (!yieldElement || !yieldElement.childNodes || !yieldElement.childNodes[0]) {
                console.log(`Error: Missing ${element.tagName} element`);
                missingElements = true;
                yields.push([null]); // Push null for missing elements
            } else {
                const yieldValue = parseFloat(yieldElement.childNodes[0].data);
                yields.push([Math.round(yieldValue * 100) / 100]);
            }
        });
        
        if (missingElements) {
            console.log('Warning: Some yield elements were missing. Returning partial data.');
        }
        
        return yields;
    } catch (error) {
        console.log('Error parsing yields XML:', error);
        return null;
    }
}

function appendLatestYieldsToAllYields(latestYields, allYields) {
    //
    // * Calculate the absolute and percentage differences between previous and current yields.
    // * Add values to a new object and store in allYields variable.

    // This will find the most recent yields which are currently available.
    const previousYields = allYields[allYields.length - 1];

    if (latestYields[0] <= allYields[allYields.length - 1][0]) {
        console.log('Checked for updated yields, but didn\'t find any');
        return false;
    }

    if (latestYields.length !== previousYields.length) {
        console.log('latest and previous yields have different number of yields');
        console.log('allYields.length: ' + allYields.length);
        console.log('latestYields.length: ' + latestYields.length);
        console.log('previousYields.length: ' + previousYields.length);
        return false;
    }
    console.log('Updating yields with the latest');
    latestYields.forEach((latestYield, i) => {
        latestYield[1] = Math.round((latestYield[0] - previousYields[i][0]) * 100) / 100.0;
        const percentChange = util.percentageChange(previousYields[i][0], latestYield[0]);
        latestYield[2] = isNaN(percentChange) ? null : Math.round(percentChange * 100) / 100.0;
    });

    allYields.push(latestYields);
    return true;
}

// Updated to use AWS SDK v3
async function loadYields(callback) {
    const params = {
        Bucket: 'yield.io',
        Key: 'api/allYields.json'
    };
    
    try {
        const command = new GetObjectCommand(params);
        const response = await s3Client.send(command);
        // Convert the stream to a string
        const bodyContents = await streamToString(response.Body);
        callback(null, JSON.parse(bodyContents));
    } catch (err) {
        console.log('Error loading yields from S3:', err);
        callback(err);
    }
}

// Helper function to convert stream to string
function streamToString(stream) {
    return new Promise((resolve, reject) => {
        const chunks = [];
        stream.on('data', (chunk) => chunks.push(chunk));
        stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf-8')));
        stream.on('error', reject);
    });
}

// Updated to use AWS SDK v3
function fetchAndUpdateYields(allYields, callback) {
    let yieldXML = "";
    console.log('Fetching bond yields from URL:', config.bondYields.url);
    
    // Use https.get with options object to enable following redirects manually
    const req = https.get(config.bondYields.url, (res) => {
        console.log(`Response status: ${res.statusCode}, headers:`, res.headers);
        
        // Handle redirects (301, 302, 307, 308)
        if (res.statusCode >= 300 && res.statusCode < 400 && res.headers.location) {
            console.log(`Following redirect to: ${res.headers.location}`);
            
            // Make a new request to the redirect location
            https.get(res.headers.location, (redirectRes) => {
                console.log(`Redirect response status: ${redirectRes.statusCode}`);
                redirectRes.setEncoding('utf8');
                
                redirectRes.on('data', (chunk) => {
                    yieldXML += chunk;
                });
                
                redirectRes.on('end', async () => {
                    processYieldXML(yieldXML);
                });
            }).on('error', (err) => {
                console.log('Error following redirect:', err);
                if (typeof callback === 'function') {
                    callback(err);
                }
            });
        } else {
            // No redirect, process the response directly
            res.setEncoding('utf8');
            res.on('data', chunk => yieldXML += chunk);
            res.on('end', async () => {
                processYieldXML(yieldXML);
            });
        }
    });
    
    req.on('error', err => {
        console.log('Error fetching yields:', err);
        if (typeof callback === 'function') {
            callback(err);
        }
    });
    
    // Helper function to process the XML response
    async function processYieldXML(xml) {
        // Log the response to debug XML issues
        console.log('Bond yields response received.');
        console.log('Response length:', xml.length);
        
        if (xml.length > 0) {
            console.log('First 200 chars of response:', xml.substring(0, 200));
        } else {
            console.log('Response is empty');
        }
        
        const parsedYields = parseYields(xml);
        if (!parsedYields) {
            console.log('Failed to parse yields from XML');
            // Check if callback is a function before calling it
            if (typeof callback === 'function') {
                return callback(new Error('Failed to parse yields from XML'), exports.YieldSpread);
            } else {
                console.log('Warning: callback is not a function');
                return; // Just return if no callback is provided
            }
        }
            
        if (appendLatestYieldsToAllYields(parsedYields, allYields)) {
            // TODO: copy the old file to a temp file in case
            //       there is an error writing the new file.
            const params = {
                Bucket: 'yield.io',
                Key: 'api/allYields.json',
                Body: JSON.stringify(allYields),
                ACL: 'public-read'
            };
            
            try {
                // Upload to S3 using SDK v3
                const uploadCommand = new PutObjectCommand(params);
                await s3Client.send(uploadCommand);
                
                // Invalidate CloudFront using SDK v3
                const invalidationParams = {
                    DistributionId: 'E3OMDZWH4SO160',
                    InvalidationBatch: {
                        CallerReference: '' + new Date().getTime(),
                        Paths: {
                            Quantity: 1,
                            Items: ['/*']
                        }
                    }
                };
                
                const invalidationCommand = new CreateInvalidationCommand(invalidationParams);
                await cloudfrontClient.send(invalidationCommand);
                
                // Check if callback is a function before calling it
                if (typeof callback === 'function') {
                    return callback(undefined, exports.YieldSpread);
                } else {
                    console.log('Yields updated successfully');
                }
            } catch (err) {
                console.log('Error updating yields:', err);
                // Check if callback is a function before calling it
                if (typeof callback === 'function') {
                    return callback(err);
                }
            }
        } else {
            // If no new yields were appended, still consider it a success
            console.log('No new yields to append');
            // Check if callback is a function before calling it
            if (typeof callback === 'function') {
                return callback(undefined, exports.YieldSpread);
            }
        }
    }
}

// Check the config to see if the URL is correct
console.log('Bond yields URL from config:', config.bondYields.url);

exports.updateYields = () => loadYields((err, data) => {
    if (err) {
        console.log(err);
    } else {
        console.log('loaded all yields');
        exports.YieldSpread = data;
        fetchAndUpdateYields(exports.YieldSpread, yieldsUpdatedCallback);
    }
});

