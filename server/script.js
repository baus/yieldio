// Load environment variables from .env file
require('dotenv').config();

// No need to globally configure AWS with v3 as each client will be configured individually
// The data module will handle its own AWS SDK v3 configuration

const data = require('./lib/data.js');
data.updateYields();

